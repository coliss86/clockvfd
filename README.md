# clockVFD

Arduino sketch based on SPI_VFD to display a clock on 2 lines with custom numbers

![](resources/vfd.jpg) 

Datasheet of the screen [20T202DA2JA.pdf](resources/20T202DA2JA.pdf)
