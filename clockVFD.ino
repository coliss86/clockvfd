/*
  SPI_VFD Library - Custom Character
 
 Demonstrates the use a 20x2 VFD display.  The SPI_VFD
 library works with all VFD displays that are compatible with the 
 NEC PD16314 driver and has the SPI pins brought out
 
 This sketch prints "0 1 2 3 4 5 6 7 8 9" to the VFD

  The circuit:
 * VFD Data to digital pin 2 : SIO #3
 * VFD Clock to digital pin 3 : SCK #5
 * VFD Chip select to digital pin 4 : /STB #4
 * VFD VCC (power) to 5V
 * VFD Ground (power) to Ground
 
 Library originally added 18 Apr 2008
 by David A. Mellis
 library modified 5 Jul 2009
 by Limor Fried (http://www.ladyada.net)
 example added 9 Jul 2009
 by Tom Igoe
 modified 22 Nov 2010
 by Tom Igoe
 
 This example code is in the public domain.
 */

// include the library code:
#include <SPI_VFD.h>

// initialize the library with the numbers of the interface pins
SPI_VFD vfd(2, 3, 4);

//
//      A
//     ---
//  F |   | B
//     -G-
//  E |   | C
//     ---
//      D

#define ABG 0x0
byte abg[8] = {
  B11111,
  B11111,
  B00011,
  B00011,
  B00011,
  B00011,
  B11111,
  B11111,
};

#define ED 0x1
byte ed[8] = {
  B11000,
  B11000,
  B11000,
  B11000,
  B11000,
  B11000,
  B11111,
  B11111,
};

#define ABF 0x2
byte abf[8] = {
  B11111,
  B11111,
  B11011,
  B11011,
  B11011,
  B11011,
  B11011,
  B00000,
};

#define CDE 0x3
byte cde[8] = {
  B11011,
  B11011,
  B11011,
  B11011,
  B11011,
  B11011,
  B11011,
  B11111,
};

#define AFG 0x4
byte afg[8] = {
  B11111,
  B11111,
  B11000,
  B11000,
  B11000,
  B11000,
  B11111,
  B11111,
};

#define ABGF 0x5
byte abgf[8] = {
  B11111,
  B11111,
  B11011,
  B11011,
  B11011,
  B11011,
  B11111,
  B11111,
};

#define AB 0x6
byte ab[8] = {
  B11111,
  B11111,
  B00011,
  B00011,
  B00011,
  B00011,
  B00011,
  B00000,
};

#define BG 0x7
byte bg[8] = {
  B00011,
  B00011,
  B00011,
  B00011,
  B00011,
  B00011,
  B11111,
  B11111,
};

#define FONT_UP 0
#define FONT_DOWN 1

byte font[10][2] = {
  { ABF, CDE }, // 0
  { 0x17, 0x17 }, // 1
  { ABG, ED }, // 2
  { ABG, BG }, // 3
  { CDE, 0x17 }, // 4
  { AFG, BG }, // 5
  { AB, ABGF }, // 6
  { AB, 0x17 }, // 7
  { ABGF, CDE }, // 8
  { ABGF, BG }, // 9
}; 

// setCursor(col, line)

void setup() {
  // set up the VFD's number of columns and rows: 
  vfd.begin(20, 2);

  vfd.createChar(ABG, abg);
  vfd.createChar(ED, ed);
  vfd.createChar(ABF, abf);
  vfd.createChar(CDE, cde);
  vfd.createChar(AFG, afg);
  vfd.createChar(ABGF, abgf);
  vfd.createChar(AB, ab);
  vfd.createChar(BG, bg);

  for (int i = 0; i<10; i++) {
      number(i,i*2);
  }

//  number(1,6);
//  number(4,7);
//  number(2,9);
//  number(6,10);
}

void number(uint8_t number, uint8_t col) {
  vfd.setCursor(col, 0);
  vfd.write(font[number][FONT_UP]);
  vfd.setCursor(col, 1);
  vfd.write(font[number][FONT_DOWN]);
}

void loop() {

}
